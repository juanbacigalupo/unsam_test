/*
 * Tasks.c
 *
 *  Created on: May 12, 2021
 *      Author: UNSAM EDIII
 *      Source destinado al trabajo con tareas de nuestro Sistema Operativo (RTOS).
 */

#include "Tasks.h"

unsigned int Entrada_Filtrada;
unsigned int Salida_LED;


void Lectura_Entradas (void)
{
	static unsigned int Cuenta_Estados = 0;

	static unsigned int Estado_Anterior = 0xFF;
	unsigned int Estado_Actual;


	Estado_Actual = HAL_GPIO_ReadPin(Switch_B1_GPIO_Port, Switch_B1_Pin);

	if (Estado_Actual == Estado_Anterior  &&  Cuenta_Estados < THRESHOLD)
		Cuenta_Estados ++;

	else
	{
		Estado_Anterior = Estado_Actual;
		Cuenta_Estados = 0;
	}

	if (Cuenta_Estados >= THRESHOLD)
		Entrada_Filtrada = Estado_Actual;
}


void Escritura_Salidas (void)
{
	if (Salida_LED)
		HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_SET);

	else
		HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_RESET);
}


void Error (void)
{
	while (1);
}
